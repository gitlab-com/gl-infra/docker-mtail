FROM golang:alpine AS builder
WORKDIR /go/src/github.com/google/mtail
COPY .mtail_version /tmp/.mtail_version
RUN apk add --update git make && \
    mtail_version=$(cat /tmp/.mtail_version) && \
    git clone https://github.com/google/mtail.git --branch ${mtail_version} --single-branch /go/src/github.com/google/mtail && \
    make depclean && make install_deps && PREFIX=/go make STATIC=y -B install version=${mtail_version}-gitlab

FROM alpine
COPY --from=builder /go/bin/mtail /usr/bin/mtail
ENTRYPOINT ["/usr/bin/mtail"]
EXPOSE 3903
WORKDIR /tmp
